==============
How To Section
==============

Get a Client Credential Token
-----------------------------

This tutorial explain how to get a Client Credential Token from ApiGo. To create any token (Client Credential or Authorization Code) from ApiGo, you need to create a `Client application`_.  The tutorial we are using Postman Tool for create token. On Postman Tool, any endpoint request section you can select Authorization and client oAuth 2.0 type authorization.

.. _Client application: https://apigo-documentation.readthedocs.io/en/latest/Clients.html#create-an-application

.. figure:: ../docs/images/Set_Authorization_Type_on_Postman.png
    
    Set Authorization Type on Postman

After that, click Get New Access Token button. Get New Access Token screen should be open. Please fill form with your client application information.

* Token Name: Give nay name to identify your tone on Postman.
* Grant Type:  Client Credentials
* Access Token URL: Give you Authorization server URL. 

You can find Access Token URL by Login developer portal and click Application section.

.. figure:: ../docs/images/Client_ID_Type_your_Client_Id.png

    Client ID: Type your Client Id

.. figure:: ../docs/images/Copy_ClientID.png

You can copy your client Id from Developer portal and Application section. Click Id and Id should coped on clipboard.

* Client Secret: Please, type your client secret.
* Scope: Please type your scopes by coping on Developer portal.
* Client Authorization: Send client credentials in body

    Click Request Token button

.. figure:: ../docs/images/Click_Request_Token_button.png

Authorization server generate a tone and share with you like that. Please click Use Token button.

.. figure:: ../docs/images/Click_Request_Token_button2.png
