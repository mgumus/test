========
Messages
========

Send a Message
--------------

The messages coming from the developer portal website will be viewed in this box. “Contact Us” function in the developer portal provides to send a message to you.

.. image:: ../docs/images/MessagesImage.png
    :width: 600

View the Messages
-----------------

This function provides to monitor the messages coming from developers. You can see message details and manage them. Click on the message to view it.

.. image:: ../docs/images/MessagesImage2.png
    :width: 425
