===============
Getting Started
===============

ApiGo enables you to manage all critical aspects of PSD2 with a central token providing you an exclusive customer experience with a single fully secure API on a cloud-hosted platform.

* Third-Party Program (TPP) Management 
* Consent Management 
* DDoS (Distributed Denial-of-Service) Protection 
* Centralized Token 
* Monitoring and Reporting Endpoint Usages

.. image:: ../docs/images/22242-[Converted].png

Quick start video
-----------------

Get PSD2 complied quickly saving money and time! We offer a SaaS solution that solves the difficult PSD2 compliance task for banks enabling new business models and opportunities via open banking.

This screencast will help you get started. To view our quick start video, please `click on`_.

.. _click on: https://www.youtube.com/watch?v=dXZPOB1DHhY&t=15s
