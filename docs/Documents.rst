=========
Documents
=========

This page is created for a quick and concise reference containing what developers need to know about work with your bank. It details functions, classes, return types, and more.

With this function, the user can add a category, edit existed ones, and sort them easily. Documentations which take place in the category can be created, edited, and updated.

.. image:: ../docs/images/Documents1.png

Add a Category
--------------

To add a category, enter the category name and add new documents to each category or update the documents. Also, you can optionally select a category icon. Category activation can be managed in category details. The user adds a new document by clicking the “Add Category” button; here the user gives the document a title. 

To add a category, click on the “Add Category” button. So, you will welcome the category details. Before creating a document, you should add a category. You can enter the category name and optionally select a category icon. 

.. note::

    Markdown is a text-to-HTML conversion tool for web writers. Markdown allows you to write using an easy-to-read, easy-to-write plain text format, then convert it to structurally valid XHTML (or HTML). The goal for Markdown's formatting syntax is to be as readable as possible.

.. image:: ../docs/images/CategoryDetail.png

Add a Document
--------------

After the user creates a category, documents can be generated under the document. Click on the “Add Document” button to start to write your document, and Document Details will welcome you. You can select the category on this page and give a title for the document. Also, you can manage the status of the document. 

The user fills the left side of the passage, and edited text will be the right side of the page. The user can see how the version will be reflected with Markdown. The editor uses Markdown to generate the document.

Your document will be viewed in the developer portal, like the following figure.

.. image:: ../docs/images/DocumentDetail2.png

After you add the document, it will be available in developer portal.

.. image:: ../docs/images/DocumentDetail3.png
