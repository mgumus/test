=========
Dashboard
=========

After you add your bank and create an environment, the dashboard will be accessible. You can monitor daily, weekly, monthly, and yearly statistics. Also, the filter button provides to add constraint(s) to specify your statistics. Based on your filter selections, you can view traffic overview, status code, and HTTP method statistics.

.. image:: ../docs/images/Dashboard.png

Filter Selection
----------------

This function provides to add a limit for your statistics. You can customize your statistics by a client, endpoint, status code, and HTTP method. Select which one is proper for you and monitor the statistics with these constraints.

.. image:: ../docs/images/FilterSelection_Image.png
    :width: 425

Language Selection
------------------

You can use ApiGo with two different language option, which is Turkish and English. In the coming days, ApiGo can be used with many other language options.

.. image:: ../docs/images/Language_Image.png
    :width: 425

Depends on your language selection, the management portal will be shaped.
