==================================
ApiGo: An End-to-End SaaS Solution 
==================================

Open banking business demands you to understand so many technical consepts, security issues, reports that you must solve, expecting you to allocate time and resources. ApiGo offers you and end-to-end solution to solve your open banking requirements depending on whichever standard you need to comply with, either it is UK Open Banking Group or Berlin Group standards, we have ready templates for your transition.

.. image:: ../docs/images/86-[Converted].png

User Manual
-----------

Are you new to ApiGo or are you looking to use our End-to-End SaaS Solution?
Learn about ApiGo to help you create fantastic customer journey for your business.

This documentation has been prepared for how can you use ApiGo. If you have not get in contact with us, please `click on`_.

.. _click on: http://staging-managementportal.architechtlab.com/#/

.. toctree::
   :hidden:
   :caption: User Manual

   GettingStarted
   Registration
   Environment
   Dashboard
   Messages
   Configurations
   Endpoints
   Clients
   Documents
   Agreements
   HowToSection









