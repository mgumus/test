============
Environments
============

On the main page, you can view all the functions. You can list your environments in the upper left corner. Users can create an environment or switch between existing environments. Also, you can view the status of the selected environment.

.. image:: ../docs/images/ManageEnvironment_Images.png
  :width: 425

Add a New Environment
~~~~~~~~~~~~~~~~~~~~~

To add an environment, you can use this page. Click on the “Add Environment” button and go back to the addition of an environment page. With the search line, you can find which environment will you work in.

.. image:: ../docs/images/AddanEnvironment_Images.png
  :width: 425

To edit the existing environment, click on the “edit” button. You will welcome the update environment page. Also, click on the icons to reach the developer portal website, identify server, and endpoint gateway pages.

.. image:: ../docs/images/EnvironmentDetails_Image.png
  :width: 425

Update Environment
~~~~~~~~~~~~~~~~~~

Click on "Details of Environment" to edit existed ones. To update environments, click on the “edit” button. The environment name and description can be rewritten with this page.  

.. image:: ../docs/images/UpdateEnv_Image.png
  :width: 425

You can change the status of the environment to active or not.

.. image:: ../docs/images/UpdateEnv_Image2.png
  :width: 425
