==========
Agreements
==========

With this function, you can manage your agreements. Add a new category or select from existed ones like what did you do in documents. 

Add an Agreement
----------------

.. image:: ../docs/images/Agreements1.png

From the categories, select one and click on it. After that, again, click on the “Add Agreement” button. You will welcome with Agreement Details. Each agreement should be in a category and have a title, so select a category and write a title for your agreement. You can adjust the status of the agreement and the required option. The same writing method will be used like documents.

.. image:: ../docs/images/Agreements2.png

The generated document will be available in the developer portal.

.. image:: ../docs/images/Agreements3.png
