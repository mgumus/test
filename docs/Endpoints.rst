=========
Endpoints
=========

.. note::

    The API endpoint is the point of entry in a communication channel when two systems are interacting. It refers to touchpoints of the communication between an API and a server. The endpoint can be viewed as the means from which the API can access the resources. They need from a server to perform their tasks. An API endpoint is a term for a URL of a server or service.
    Understanding how each API is performing can drastically change the way you’re able to capture the value APIs add to your business. Proactively Monitoring APIs can ensure that you’re ready to find issues before real users experience them.

To add an endpoint, you can click on it. You can also sort the list and manage them. To view Endpoint history, click on the icon, which takes place right of the “Endpoints”.

.. image:: ../docs/images/endpoint.png

Add an Endpoint
---------------

The endpoints indicate how you access the resource, while the method suggests the allowed interactions (such as GET, POST, PUT or DELETE) with the resource. You can choose the HTTP method and add the authorization type. Also, you need to add Endpoint and Destination Paths. The endpoint shows the end path of a resource URL only, not the base path common to all endpoints.

You can select the product and add the policies. The endpoints are created with Mock, Rate Limit, Cache, Account Access Permission, Account Transactions Permission, Payment Permission, JUST Transformation, Client Signature Validation, Bank Signature Validation, and Pay Limit. 

.. image:: ../docs/images/addanendpoint.png

You can add a destination point or write it manually with mock policy. If you write a mock policy, click on the “Save” button. 

.. image:: ../docs/images/Mock.png

You can add a Rate Limit policy to avoid overloading the system. Enter the “Max Count” to the relevant blank. Also, you can add a message when the order is overloaded.

.. image:: ../docs/images/RateLimit.png

After you add policies, you can publish changes with the relevant button. Finally, you can save the changes and publish changes with a version message. The generated endpoint is available to use.

.. image:: ../docs/images/publish.png

Test an Endpoint
----------------

You can check your endpoint with Postman. Get a new access token from Postman. Go to your identity server and take Auth URL (The endpoint for authorization server. This is used to get the authorization code), Access Token URL (The endpoint for the authentication server. This is used to exchange the authorization code for an access token)to take an access token.  From the clients, you can take Client ID, Client Secret, and Scope.

.. image:: ../docs/images/Postman1.png

You can request to the token. Click on “Use token”.

.. image:: ../docs/images/Postman2.png

When you write your URL with an endpoint path, and select the HTTP method, click on the “Send” button. The body will be available. You can correct the endpoint. Also, send the request too much time and check the rate limit policy.

.. image:: ../docs/images/Postman3.png
